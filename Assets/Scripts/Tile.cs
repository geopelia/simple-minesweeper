﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TileState
{
    idle,
    uncovered,
    flagged,
    detonated
}
public class Tile : MonoBehaviour
{
    // Start is called before the first frame update
    public bool isMined = false;
    public static Color defaultColor;
    static Color lightupColor;
    public int id;
    public int adjacentMinesCount = 0;
    public List<Tile> adjacentTiles;
    public static Sprite defaultSprite;
    static Sprite flaggedSprite;
    static Sprite uncoverSprite;
    static Sprite explosionSprite;
    public TileState tileState;

    static Texture2D textureFlag;
    static Texture2D textureUncover;

    static Texture2D textureExplosion;

    public SpriteRenderer spriteRenderer;

    static KeyCode[] hackCode;
    int keyPressedIndex = 0;

    bool showHack = false;

    public static bool mineExploted = false;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        adjacentTiles = new List<Tile>();        
        hackCode = new KeyCode[] { KeyCode.RightShift, KeyCode.H };
        defaultColor = spriteRenderer.color;
        lightupColor = new Color(0.9139486f, 0.9245283f, 0.3096298f, 0.8352941f);
        
    }
    void Start()
    {
        mineExploted = false;        
        tileState = TileState.idle;
        SetTileSprites();       
        

    }

    // Update is called once per frame
    void Update()
    {
     
        CheckHack();
    }

    void FixedUpdate()
    {
        if (mineExploted && isMined)
        {
            ExplodeExternal();
        }

    }

    void SetTileSprites()
    {
        TileTextures tileTextures = GameObject.Find("Grid").GetComponent<TileTextures>();
        textureFlag = tileTextures.textureFlag;
        textureUncover = tileTextures.textureUncover;
        textureExplosion = tileTextures.textureExplosion;
        defaultSprite = spriteRenderer.sprite;
        flaggedSprite = Sprite.Create(textureFlag, spriteRenderer.sprite.rect, new Vector2(0.5f, 0.5f), spriteRenderer.sprite.pixelsPerUnit);
        uncoverSprite = Sprite.Create(textureUncover, spriteRenderer.sprite.rect, new Vector2(0.5f, 0.5f), spriteRenderer.sprite.pixelsPerUnit);
        explosionSprite = Sprite.Create(textureExplosion, spriteRenderer.sprite.rect, new Vector2(0.5f, 0.5f), spriteRenderer.sprite.pixelsPerUnit);
    }

    void OnMouseOver()
    {
        if (GridGenerator.gameState == GameState.inGame) {
            if (tileState == TileState.idle || tileState == TileState.flagged)
            {
                spriteRenderer.color = lightupColor;
            }

            if (tileState == TileState.idle)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    SetFlag();
                }
                if (Input.GetMouseButtonDown(0))
                {
                    UncoverTile();
                }
            }
            else if (tileState == TileState.flagged)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    SetFlag();
                }
            }

        }    


    }

    void OnMouseExit()
    {
        if (GridGenerator.gameState == GameState.inGame) {
            if (tileState == TileState.idle || tileState == TileState.flagged)
            {
                spriteRenderer.color = defaultColor;
            }
        }
        

    }

    public void SetFlag()
    {
        if (tileState == TileState.idle)
        {
            tileState = TileState.flagged;
            spriteRenderer.sprite = flaggedSprite;
            GridGenerator.minesRemaining-=1;
            if (isMined) {
                GridGenerator.minesMarkedCorrectly++;
            }

        }
        else if (tileState == TileState.flagged)
        {
            tileState = TileState.idle;
            spriteRenderer.sprite = defaultSprite;
            GridGenerator.minesRemaining++;
            if (isMined) {
                GridGenerator.minesMarkedCorrectly -=1;
            }

        }
    }

    void UncoverTile()
    {
        if (!isMined)
        {
            tileState = TileState.uncovered;
            spriteRenderer.sprite = uncoverSprite;
            
            ShowTextComponent();
            GridGenerator.tilesUncovered++;            
            if (adjacentMinesCount == 0)
            {
                UncoverAdjacentTiles();
                spriteRenderer.color = Color.green;
            } else {
                spriteRenderer.color = defaultColor;
            }
        }
        else
        {
            Explode();
        }
    }

    void Explode()
    {
        // Debug.Log("Fin del juego");
        GridGenerator.gameState = GameState.gameOver;
        tileState = TileState.detonated;
        spriteRenderer.sprite = explosionSprite;
        mineExploted = true;
    }

    void ExplodeExternal()
    {
        tileState = TileState.detonated;
        spriteRenderer.sprite = explosionSprite;
    }

    void ShowTextComponent()
    {
        // mostrar texto
        GameObject textGameObject = GameObject.Find("Texto" + id);
        if (textGameObject != null)
        {
            textGameObject.GetComponent<Text>().enabled = true;
        }
    }

    void UncoverAdjacentTiles()
    {
        foreach (Tile currentTile in adjacentTiles)
        {
            if (!currentTile.isMined && currentTile.tileState == TileState.idle)
            {
               
                if (currentTile.adjacentMinesCount == 0)
                {
                    currentTile.UncoverTile();
                }
                else if (currentTile.adjacentMinesCount > 0)
                {
                    currentTile.UncoverTileExternal();
                }

            }
        }
    }

    public void UncoverTileExternal()
    {
        tileState = TileState.uncovered;
        spriteRenderer.sprite = uncoverSprite;

        ShowTextComponent();
        GridGenerator.tilesUncovered++;
    }

    void CheckHack()
    {
        //Debug.Log(" index  " + keyPressedIndex + "  valor " +hackCode[keyPressedIndex] );
        if (Input.anyKeyDown)
        {
            if (Input.GetKeyDown(hackCode[keyPressedIndex]))
            {
                keyPressedIndex++;
                if (keyPressedIndex == hackCode.Length)
                {
                    ShowHack();
                    keyPressedIndex = 0;
                }
            }
            else
            {
                keyPressedIndex = 0;
            }
        }
    }

    void ShowHack()
    {
        showHack = !showHack;
        if (tileState == TileState.idle)
        {
            if (isMined && showHack)
            {
                GetComponent<SpriteRenderer>().color = Color.red;
            }
            else
            {
                GetComponent<SpriteRenderer>().color = defaultColor;
            }
        }

    }
}
