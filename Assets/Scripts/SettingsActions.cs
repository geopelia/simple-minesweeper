﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Text;

public class SettingsActions : MonoBehaviour
{
    public static SettingsActions instance;
    public InputField inputRows;
    public InputField inputCols;
    public InputField inputMineNumbers;
    public Dropdown comboLanguages;
    public const String ROWS_KEY = "MS_ROWS";
    public const String COLS_KEY = "MS_COLUMNS";
    public const String MINES_KEY = "MS_MINENUMBERS";
    public const String LANG_KEY = "MS_LANGUAGE";
    public Canvas msgCanvas;
    public Text messageLabel;
    public GameObject backGroundPanel;
   

    void Awake() {
        if (instance == null) {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        } else {
            Destroy(this);			
        }
    }

    void Start()
    {
        int tilesPerRow = PlayerPrefs.GetInt(SettingsActions.COLS_KEY);
        if (tilesPerRow == 0) {
            tilesPerRow = 9;
        }
        int cols = PlayerPrefs.GetInt(SettingsActions.ROWS_KEY);
        if (cols == 0) {
            cols = 9;
        }
        int numberOfMines = PlayerPrefs.GetInt(SettingsActions.MINES_KEY);
        if (numberOfMines == 0) {
            numberOfMines = 10;
        } 
        inputRows.text = cols.ToString("D");
        inputCols.text = tilesPerRow.ToString("D");
        inputMineNumbers.text = numberOfMines.ToString("D");
        // int lang = PlayerPrefs.GetInt(SettingsActions.LANG_KEY);
        int lang = (int)LabelTranslator.instance.CurrentLang;
        try {
            comboLanguages.value = lang;
        } catch (Exception e) {
            Debug.Log("error setting language" + e.Message);
        }
        
    }


    public void CancelSave() {
        SceneManager.LoadScene(MainMenuNavigator.MAIN_MENU_SCENE);
    }

    public void SaveSettings() {
        String rowsStr = inputRows.text;
        String colsStr = inputCols.text;
        String mineNumberStr = inputMineNumbers.text;
        StringBuilder errorMessages = new StringBuilder();
        LanguagesUsed language = (LanguagesUsed) comboLanguages.value;       

        if (String.IsNullOrEmpty(rowsStr)) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorEmptyRow", language));
           
        }
        if (String.IsNullOrEmpty(colsStr)) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorEmptyCol", language));
           
        }
        if (String.IsNullOrEmpty(mineNumberStr)) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorEmptyMine", language));
           
        }
        if (errorMessages.Length > 1) {
            ShowErrorCanvas(errorMessages.ToString(), true);
            return;
        }
        int rows = 0;
        int cols = 0;
        int minesNumber = 0;
        try
        {
            rows = Int32.Parse(rowsStr);
            cols = Int32.Parse(colsStr);
            minesNumber = Int32.Parse(mineNumberStr);
        }
        catch (FormatException ex)
        {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorParseField", language) + ex.Message);
        }
        if (errorMessages.Length > 1) {
            ShowErrorCanvas(errorMessages.ToString(), true);
            return;
        }
        if (rows < 1) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorRowCount", language));
        } 
        if (cols < 1) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorColCount", language));
        }
        if (minesNumber < 1) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorMineCount", language));
        }
        if (rows > 9) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorMaxRow", language));
        }
        if (cols > 12) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorMaxCol", language));
        }
        if (minesNumber >= (rows * cols)) {
            errorMessages.AppendLine(LabelTranslator.ResolveStringValue("errorMineMax", language));
        }
        if (errorMessages.Length > 1) {
            ShowErrorCanvas(errorMessages.ToString(), true);
            return;
        }
        PlayerPrefs.SetInt(ROWS_KEY, rows);
        PlayerPrefs.SetInt(COLS_KEY, cols);
        PlayerPrefs.SetInt(MINES_KEY, minesNumber);
        PlayerPrefs.SetInt(SettingsActions.LANG_KEY, comboLanguages.value);
        PlayerPrefs.Save();
        if (LabelTranslator.instance.CurrentLang != language){
            LabelTranslator.instance.CurrentLang = language;
            LabelTranslator.instance.ChangedLang = true;
        } else {
            LabelTranslator.instance.ChangedLang = false;
        }       
        ShowErrorCanvas(LabelTranslator.ResolveStringValue("msgSaved", language), false);
    }

    public void Accept() {
        msgCanvas.enabled = false;
    }

    void ShowErrorCanvas(string messages, bool isError) {
        msgCanvas.enabled = true;
        Image msgBackground = backGroundPanel.GetComponent<Image>();       
        if (msgBackground != null) {
            if (isError) {
                msgBackground.color =  Color.red;
            } else {
                msgBackground.color =  new Color(0.2726769f, 0.6981132f, 0.06915272f, 1f);
            }
            
        }
        messageLabel.text = messages;
    }

   
}
