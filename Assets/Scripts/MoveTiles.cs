﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTiles : MonoBehaviour
{
    // Start is called before the first frame update
    GridGenerator grid;
    int numberOfTiles;
    int tilesPerRow;
    Tile[] tilesAll;

    void Start()
    {
        grid = GetComponent<GridGenerator>();
        numberOfTiles = grid.numberOfTiles;
        tilesPerRow = grid.tilesPerRow;
        tilesAll = grid.tilesAll;
        CalculateGridSize();
    }

    public void CalculateGridSize()
    {
        bool isRectangle = (numberOfTiles > 0 && numberOfTiles % tilesPerRow == 0);

        int rowNumber = (numberOfTiles / tilesPerRow);
        int maxNumberInRow = tilesPerRow;
        if (rowNumber == 0)
        {
            maxNumberInRow = numberOfTiles % tilesPerRow;
        }

        int position = 0;
        Vector3 topLeftPosition = tilesAll[position].gameObject.transform.position;
        Vector3 bottomLeftPosition = Vector3.zero;
        Vector3 topRightPosition = Vector3.zero;
        Vector3 bottomRightPosition = Vector3.zero;
        position = maxNumberInRow - 1;
        checkAndAssignValueFromVector(position, ref topRightPosition);
        if (isRectangle)
        {
            position = (rowNumber - 1) * maxNumberInRow;
            checkAndAssignValueFromVector(position, ref bottomLeftPosition);

            position = numberOfTiles - 1;
            checkAndAssignValueFromVector(position, ref bottomRightPosition);

        }
        else
        {
            position = (rowNumber) * maxNumberInRow;
            checkAndAssignValueFromVector(position, ref bottomLeftPosition);
            bottomRightPosition = new Vector3(topRightPosition.x, bottomLeftPosition.y, bottomLeftPosition.z);
        }


        // Debug.Log("posiciones: " + topLeftPosition + " " + topRightPosition + " " + bottomLeftPosition + " " + bottomRightPosition);
        //DrawOutline(topLeftPosition, topRightPosition, bottomLeftPosition, bottomRightPosition);
        Vector3 offset = FindGridCenter(topLeftPosition, bottomRightPosition);

        MoveElements(offset);


    }

    void checkAndAssignValueFromVector(int index, ref Vector3 position)
    {
        if (grid.InBounds(index))
        {
            position = tilesAll[index].gameObject.transform.position;
        }
        else
        {
            Debug.LogError("fallo indice" + index);
        }
    }

    void DrawOutline(Vector3 topLeftPosition, Vector3 topRightPosition, Vector3 bottomLeftPosition, Vector3 bottomRightPosition)
    {
        Debug.DrawLine(topLeftPosition, topRightPosition, Color.red, 5, false);
        Debug.DrawLine(topLeftPosition, bottomLeftPosition, Color.red, 5, false);
        Debug.DrawLine(topRightPosition, bottomRightPosition, Color.red, 5, false);
        Debug.DrawLine(bottomLeftPosition, bottomRightPosition, Color.red, 5, false);

    }

    Vector3 FindGridCenter(Vector3 topLeftPosition, Vector3 bottomRightPosition)
    {
        float avgX = (topLeftPosition.x + bottomRightPosition.x) / 2;
        float avgY = (topLeftPosition.y + bottomRightPosition.y) / 2;
        Vector3 gridCenter = new Vector3(avgX, avgY, topLeftPosition.z);
        Debug.DrawLine(gridCenter, Vector3.zero, Color.green, 5, false);
        return gridCenter;
    }

    void MoveElements(Vector3 offset)
    {
        foreach (Tile tile in tilesAll)
        {
            Vector3 correction = tile.transform.position - offset;
            tile.transform.position = correction;

        }
    }

    // Update is called once per frame
    void Update()
    {

    }

}
