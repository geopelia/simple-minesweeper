﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuNavigator : MonoBehaviour
{
    public const int MAIN_MENU_SCENE = 0;
    public const int GAME_SCENE = 2;
    public const int SETTINGS_SCENE = 1;
    public void GotoGame(){
        SceneManager.LoadScene(GAME_SCENE);
    }

    public void GotoSettings() {
        SceneManager.LoadScene(SETTINGS_SCENE);
    }

    public void ExitGame() {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif

    }

}
