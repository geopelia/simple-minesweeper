﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public enum GameState
{
    inGame, gameOver, gameWon
}
public class GridGenerator : MonoBehaviour
{
    public Tile tilePrefab;
    public int numberOfTiles;
    public int tilesPerRow = 4;
    public int numberOfMines = 10;
    public float distanceBetweenTiles = 1f;

    public Tile[] tilesAll;
    public List<Tile> tilesMined;
    public List<Tile> tilesUnmined;
    public static GameState gameState;
    public Text stateText;
    public Text minesRemainingText;
    public static int minesMarkedCorrectly;
    public static int tilesUncovered;
    public static int minesRemaining;
    bool isPrinted = false;
    // Start is called before the first frame update
    void Awake()
    {
        LoadTileSettings();
        CreateTiles();
        gameState = GameState.inGame;
        minesRemaining = numberOfMines;
        minesMarkedCorrectly = 0;
        tilesUncovered = 0;        
    }

    void LoadTileSettings() {
       
        tilesPerRow = PlayerPrefs.GetInt(SettingsActions.COLS_KEY);
        if (tilesPerRow == 0) {
            tilesPerRow = 9;
        }
        int cols = PlayerPrefs.GetInt(SettingsActions.ROWS_KEY);
        if (cols == 0) {
            cols = 9;
        }
        numberOfMines = PlayerPrefs.GetInt(SettingsActions.MINES_KEY);
        if (numberOfMines == 0) {
            numberOfMines = 10;
        } 
        numberOfTiles = tilesPerRow * cols;
    }

    void PrintVisible(){
        
        foreach (Tile tile in tilesAll)
        {
            Vector3 screenPoint = Camera.main.WorldToViewportPoint(tile.transform.position);
            string message = "";
            bool condition = screenPoint.z >= 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
            if (!condition)
            {   
                message = String.Format("Object {0} is no visible,  point {1}  ",  tile.id, screenPoint);
                Debug.Log(message);
                break;
            }  
            
            
        }

    }

    // Update is called once per frame
    void Update()
    {
        // stateText.text = gameState.ToString();
        minesRemainingText.text = minesRemaining.ToString("D2");
        if (gameState == GameState.inGame)
        {
            if ((minesRemaining == 0 && minesMarkedCorrectly == numberOfMines)
            || (tilesUncovered == numberOfTiles - numberOfMines))
            {
                FinishGame();
            }
        }
        if (gameState == GameState.gameOver) {
            GameObject.Find("LoseCanvas").GetComponent<Canvas>().enabled = true;
        }
        if (!isPrinted) {
            PrintVisible();
            isPrinted = true;
        }
        

    }

    void CreateTiles()
    {
        tilesAll = new Tile[numberOfTiles];
        tilesMined = new List<Tile>();
        tilesUnmined = new List<Tile>();
        float xOffset = 0;
        float yOffset = 0;
        for (int tilesCreated = 0; tilesCreated < numberOfTiles; tilesCreated++)
        {
            xOffset += distanceBetweenTiles;
            if (tilesCreated % tilesPerRow == 0)
            {
                yOffset += distanceBetweenTiles;
                xOffset = 0;
            }
            Tile newTile = Instantiate(tilePrefab, new Vector3(transform.position.x + xOffset,
                transform.position.y - yOffset, transform.position.z),
                transform.rotation);
            newTile.id = tilesCreated;
            // newTile.tilesPerRow = tilesPerRow;
            newTile.transform.SetParent(this.transform, false);
            tilesAll[tilesCreated] = newTile;

        }
        AssignMines();
        FillAdjacentTiles();
        CountAdjacentMines();
    }

    void AssignMines()
    {
        tilesUnmined = new List<Tile>(tilesAll);
        for (int mineAssigned = 0; mineAssigned < numberOfMines; mineAssigned++)
        {
            Tile currenTile = tilesUnmined[UnityEngine.Random.Range(0, tilesUnmined.Count)];
            currenTile.GetComponent<Tile>().isMined = true;
            tilesMined.Add(currenTile);
            tilesUnmined.Remove(currenTile);

        }
    }

    void FillAdjacentTiles()
    {
        foreach (Tile tile in tilesAll)
        {
            int tileId = tile.id;
            if (InBounds(tileId + tilesPerRow)) tile.adjacentTiles.Add(tilesAll[tileId + tilesPerRow]);
            if (InBounds(tileId - tilesPerRow)) tile.adjacentTiles.Add(tilesAll[tileId - tilesPerRow]);
            if (InBounds(tileId - 1) && NotFirstOrLastInRow(tileId)) tile.adjacentTiles.Add(tilesAll[tileId - 1]);
            if (InBounds(tileId + 1) && NotFirstOrLastInRow(tileId + 1)) tile.adjacentTiles.Add(tilesAll[tileId + 1]);
            if (InBounds(tileId + tilesPerRow + 1) && NotFirstOrLastInRow(tileId + 1)) tile.adjacentTiles.Add(tilesAll[tileId + tilesPerRow + 1]);
            if (InBounds(tileId + tilesPerRow - 1) && NotFirstOrLastInRow(tileId)) tile.adjacentTiles.Add(tilesAll[tileId + tilesPerRow - 1]);
            if (InBounds(tileId - tilesPerRow + 1) && NotFirstOrLastInRow(tileId + 1)) tile.adjacentTiles.Add(tilesAll[tileId - tilesPerRow + 1]);
            if (InBounds(tileId - tilesPerRow - 1) && NotFirstOrLastInRow(tileId)) tile.adjacentTiles.Add(tilesAll[tileId - tilesPerRow - 1]);
        }
    }

    public bool InBounds(int position)
    {
        return position >= 0 && position < tilesAll.Length;
    }

    bool NotFirstOrLastInRow(int position)
    {
        return position % tilesPerRow != 0;

    }

    void CountAdjacentMines()
    {
        int adjacentMines = 0;
        foreach (Tile tile in tilesAll)
        {
            adjacentMines = 0;
            foreach (Tile adjacentTile in tile.adjacentTiles)
            {
                if (adjacentTile.isMined)
                {
                    adjacentMines++;
                }
            }
            tile.adjacentMinesCount = adjacentMines;

        }
    }

    void FinishGame()
    {
        gameState = GameState.gameWon;
        foreach (Tile tile in tilesAll)
        {
            if (tile.tileState == TileState.idle && !tile.isMined)
            {
                tile.UncoverTileExternal();
            }
        }
        foreach (Tile currentTile in tilesMined)
        {
            if (currentTile.tileState != TileState.flagged)
            {
                currentTile.SetFlag();
            }
        }
        GameObject.Find("WinCanvas").GetComponent<Canvas>().enabled = true;

    }


    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // public void ExitGame() {
    //     #if UNITY_EDITOR
    //     UnityEditor.EditorApplication.isPlaying = false;
    //     #else
    //     Application.Quit();
    //     #endif

    // }

    public void BackToMenu()
    {
        SceneManager.LoadScene(MainMenuNavigator.MAIN_MENU_SCENE);
    }

}
