﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public enum LanguagesUsed {
    English, Spanish
}

public class LabelTranslator : MonoBehaviour
{
    public static LabelTranslator instance;
 
    public LanguagesUsed CurrentLang {get;set;}
    public bool ChangedLang {get;set;}

    static Dictionary<LanguagesUsed, Dictionary<string, string>> Translations = new Dictionary<LanguagesUsed, Dictionary<string, string>>() {
        [LanguagesUsed.English] = new Dictionary<string, string>() {
            ["CancelTxt"] = "Go Back",
            ["SaveTxt"] = "Save",
            ["MineLbl"] = "Number of Mines:",
			["ColumnsLbl"] = "Number of Columns:",
			["RowsLbl"] = "Number of Rows:",
			["Placeholder"] = "Enter text...",
			["OkText"] = "Accept",
			["errorColCount"] = "There must be at least one column",
			["errorRowCount"] = "There must be at least one row",
			["errorMineCount"] = "There must be at least one mine",
			["errorMaxRow"] = "Maximum number of rows exceeded",
			["errorMaxCol"] = "Maximum number of columns exceeded",
			["errorMineMax"] = "The number of mines cannot exceed the number of tiles",
			["msgSaved"] = "Settings Saved",
			["errorParseField"] = "Error reading fields" ,
			["errorEmptyRow"] = "Empty Rows",
			["errorEmptyMine"] = "Empty Mine Number",
			["errorEmptyCol"] = "Empty Cols",
            ["LanguagesLbl"] = "Language:",
            ["mainLblPlay"] = "PLAY",
            ["mainLblSettings"] = "Settings",
            ["mainLblExit"] = "Exit",
            ["MinesRemainingLabel"] = "Mines Remaining:",
            ["lblVictory"] = "VICTORY",
            ["lblRestart"] = "Restart",
            ["lblGoBack"] = "Back to Menu",
            ["lblGameOver"] = "Game Over",


        },
        [LanguagesUsed.Spanish] = new Dictionary<string, string>() {
            ["CancelTxt"] = "Volver al Inicio",
            ["SaveTxt"] = "Guardar",
            ["MineLbl"] = "Cantidad de Minas:",
			["ColumnsLbl"] = "Cantidad de Columnas:",
			["RowsLbl"] = "Cantidad de filas:",
			["Placeholder"] = "Ingrese Texto...",
			["OkText"] = "Aceptar",
			["errorRowCount"] = "Debe haber al menos una fila",
			["errorColCount"] = "Debe haber al menos una columna",
			["errorMineCount"] = "Debe haber al menos una mina",
			["errorMaxRow"] = "Cantidad máxima de filas excedida",
			["errorMaxCol"] = "Cantidad máxima de columnas excedidas",
			["errorMineMax"] = "La cantidad de minas no puede exceder el número de casillas",
			["msgSaved"] = "Preferencias Guardadas",
			["errorParseField"] = "Error leyendo los campos",
			["errorEmptyRow"] = "Filas vacías",
			["errorEmptyCol"] = "Columnas vacías",
			["errorEmptyMine"] = "Cantidad de minas vacías",
            ["LanguagesLbl"] = "Idioma:",
            ["mainLblPlay"] = "JUGAR",
            ["mainLblSettings"] = "Preferencias",
            ["mainLblExit"] = "Salir",
            ["MinesRemainingLabel"] = "Minas Restantes:",
            ["lblVictory"] = "VICTORIA",
            ["lblRestart"] = "Reiniciar",
            ["lblGoBack"] = "Volver al Menú",
            ["lblGameOver"] = "Fin del Juego",

        }    
    };

    static String[] emptyMsgs = {"LblLanguages", "MessageTxt", "EmptyText", "MinesRemaining"};

    void Awake() {
        if (instance == null) {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        } else {
            Destroy(this);			
        }
        try {
            int lang = PlayerPrefs.GetInt(SettingsActions.LANG_KEY);
            CurrentLang = (LanguagesUsed) lang;            
        } catch (Exception) {
            CurrentLang = LanguagesUsed.English;
        }
        ChangedLang = false;
    }

    // Start is called before the first frame update
    void Start()
    {       
        Text[] textArr = FindObjectsOfType<Text>();       
        foreach (Text text in textArr){
            //Debug.Log(text.name);
            text.text = ResolveStringValue(text.name);
        }
        // Debug.Log(CurrentLang.ToString());
    }

    public void RefreshTexts() {
        if (ChangedLang) {
            Text[] textArr = FindObjectsOfType<Text>();       
            foreach (Text text in textArr){
                //Debug.Log(text.name);
                string value = ResolveStringValue(text.name);
                if (!String.IsNullOrEmpty(value)) {
                    text.text = value;

                }
                
            }
        }
        
    }

    string ResolveStringValue(string id) {
        try 
        {          
            return Translations[CurrentLang][id];
        } catch (KeyNotFoundException ) {
            if (!Array.Exists(emptyMsgs, element => element == id)) {
                Debug.Log("No se encontro clave: " + id  );
                return "N/A";
            }
            return "";
        }    
    }

    public static string ResolveStringValue(string id, LanguagesUsed language) {
        try 
        {       
            return Translations[language][id];
        } catch (KeyNotFoundException ) {
            
            if (!Array.Exists(emptyMsgs, element => element == id)) {
                Debug.Log("No se encontro clave: " + id  );
                return "N/A";
            }
            return "";
        }    
    }

    
   
}
