﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasExample : MonoBehaviour
{
    public GridGenerator tileGrid;
    public bool isShowText;
   
    // Start is called before the first frame update
    void Start()
    {
        Canvas myCanvas = GetComponent<Canvas>();

        Transform otroObjeto;
        foreach (Tile tile in tileGrid.tilesAll)
        {
            otroObjeto = tile.gameObject.transform;
            GameObject myText = InitialliceTextGameObject(tile.id);
            Text text = createText(ref myText, isShowText, tile.adjacentMinesCount);

            Vector3 localPositionTarget = GetTargetLocalPositionInCanvas(otroObjeto);
            // Debug.Log(localPositionTarget);

            // Text position
            RectTransform rectTransform = CreateRectTransform(ref text, localPositionTarget);

        }
    }

    // Update is called once per frame
    void Update()
    {



    }

    GameObject InitialliceTextGameObject(int number)
    {
        GameObject myText = new GameObject();
        myText.transform.SetParent(this.transform, false);
        myText.name = "Texto" + number;
        myText.layer = 5;
        return myText;
    }

    Text createText(ref GameObject myText, bool enable, int numberToShow)
    {
        Text text = myText.AddComponent<Text>();
        text.font = (Font)Resources.Load("Fonts/Ubuntu-R");                
        if (numberToShow > 0) {
            text.text = numberToShow.ToString("D2");
        }        
        text.fontSize = 18;
        text.alignment = TextAnchor.UpperCenter;
        text.enabled = enable;
        text.fontStyle = FontStyle.Bold;
        switch (numberToShow)
        {            
            case 1:
            case 2:
                text.color = Color.green;
                break;
            case 3:
            case 4:
            case 5:
                text.color = Color.yellow;
                break;
            case 6:
            case 7:
            case 8:
                text.color = Color.red;
                break;
            default:
                break;
        }
        return text;
    }

    Vector3 GetTargetLocalPositionInCanvas(Transform target)
    {
        Vector3 screenPositionTarget = Camera.main.WorldToScreenPoint(target.position);
        // Debug.Log(screenPos);      
        return this.transform.InverseTransformPoint(screenPositionTarget);

    }

    RectTransform CreateRectTransform(ref Text textComponent, Vector3 localPosition)
    {
        RectTransform rectTransform = textComponent.GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(40, 20);
        rectTransform.localPosition = localPosition;
        //  Debug.Log("global: " + rectTransform.position + " local: "+ rectTransform.localPosition);     
        return rectTransform;
    }


}
